# Tutorial for Luanti

Created by Wuzzy

Version: 4.0.1
Recommended for Luanti version 5.10.0.

(earlier version down to Luanti 5.5.0 work, but the texts are less
accurate. Translations only work from Luanti 5.10.0 onwards)

Learn how to use and play Luanti in a nice and cozy castle. Here you learn most of the basics of Luanti and how to play.

## How to install
First, you have to install Luanti.

You can download the Tutorial from the ContentDB in Luanti (“Content” tab).
Luanti will then do the rest for you.

### Manual installation
If for some reason the ContentDB doesnt work, you can still fall back to manual installation.
If you have manually downloaded the Tutorial (e.g. from the Luanti forums), here's how to install it:

Then, put this “tutorial” directory into the “games” sub-directory of your Luanti user directory.

Depending on your operating system, you have to put it into the following directory:

* GNU/Linux: $HOME/.minetest/games, where “$HOME” refers to your home directory.
* MacOS: $HOME/.minetest/games, where “$HOME” refers to your home directory.
* Windows: into the “games” sub-directory of the program folder in which you have installed Luanti. It is the directory containing the directories (not necessarily exhaustive list) `bin` (which in turn contains minetest.exe), `builtin`, `client`, `doc`, `fonts`, `games`, `locale`, `mods` and `textures`.


## How to use
Start Luanti, select “Start Game” (i.e. singleplayer) then select the “Tutorial” icon below.

Create a new world and hit “Play Game”. The Tutorial should explain the rest.

## Resetting the tutorial
In case you want to start over, you can just create a new world and play with that one instead.

## Features
The tutorial covers the following topics:

* Luanti, mods and games
* Camera
* Movement
* Sneaking
* Pointing
* Sneaking
* Mining
* Building
* Liquids
* Items, tools
* Crafting
* Health, breath, damage
* Death and respawning
* A few random hints

This tutorial is written in a fairly generic way and does not focus on any particular game for Luanti. Real games for Luanti will likely have additional or slightly different rules, but the basics will generally be the same.

If you have completed all tasks, you will be awarded a golden cup.
There's also a secret prize, can you win it?


## Legalities
This tutorial is free software. You can legally study, copy, share, transform, remix, distribute this to your liking, under certain conditions.

The mods “`tutorial`” and “`tutorial_supplemental`” as well the tutorial world are original work by Wuzzy and fall under the MIT License.
The mod “`tutorial_mapgen`” (Tutorial World generator) is also original work by Wuzzy, same license, but it contains contributions.

Contributions:

* Drachenbauer32 added reinforced wood and improved the straw room and the the red roof a bit.
* eol contributed the Dutch translation
* LauwCost contributed to the French translation

Other mods are creation by other people, but heavily modified to fit the Tutorial's needs.

The mods “`tutorial_default`” and “`tutorial_creative`” falls under the GNU LGPLv2.1 for the code and the CC BY-SA 3.0 for artwork.
The mod “`tutorial_darkage`” falls under the MIT License.
The mod “`tutorial_music`” falls under the MIT License for the code and the CC BY 3.0 for the music.
The mod “`tutorial_castle`” falls under the MIT License (the author allowed me an exception).
The mod “`tutorial_arrow_signs`” falls under the CC BY-SA 3.0.
The mod “`tutorial_areas`” falls under the GNU LGPLv2.1 (or later).
The mod “`tutorial_cottages`” falls under the GNU GPLv2 (the author allowed me an exception).
The mod “`show_wielded_item`” is licensed under the GNU LGPLv2 or later.
The mod “`tutorial_supplemental`” falls under the MIT License, with one exception:
The texture `supplemental_loudspeaker.png` falls under the CC BY-SA 3.0 (from the developers of the Mesecons mod).
Everything else falls under the MIT License.

For even more detailed information (including credits), look for README files in the respective mod directories.

### License references
* MIT License: https://mit-license.org/
* GNU GPLv2: https://www.gnu.org/licenses/gpl-2.0.html
* GNU LGPLv2.1: https://www.gnu.org/licenses/lgpl-2.1.html
* CC BY-SA 3.0: https://creativecommons.org/licenses/by-sa/3.0/
* CC BY 3.0: https://creativecommons.org/licenses/by/3.0/
