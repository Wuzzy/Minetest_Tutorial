-- This file will NOT be executed. Instead, it just
-- provides a list of all sign names for the
-- translation string collecting utilities to
-- see.

-- This list must be *manually* updated whenever a
-- sign name in the tutorial world is changed,
-- added or removed.


-- Symbol for translation string collecting tool
local NS = function(s) return s end

--- LIST OF SIGNS ---
NS("Ladders section")
NS("Swimming and Diving sections")
NS("To the Diving section")
NS("Diving section")
NS("Pointing section")
NS("Mining section, Building section (prerequisites: Item and Tools section)")
NS("Tutorial Mine (just for fun)")
NS("Special Blocks section")
NS("To the Using section")
NS("To the Items, Tools, Crafting and Smelting house")
NS("The Items, Tools, Crafting and Smelting house")
NS("Waterfall section")
NS("Swimming section, Ladders section")
NS("Sneaking section")
NS("Health and Damage section")
NS("Viscosity section")
NS("The Good-Bye Room")
NS("Exit")
NS("Comestibles room")
NS("Smelting room")
NS("Building section")
--- END OF LIST OF SIGNS ---
