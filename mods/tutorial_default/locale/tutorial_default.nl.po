msgid ""
msgstr ""
"Project-Id-Version: Tutorial (Luanti) 3.4.0\n"
"Report-Msgid-Bugs-To: Wuzzy@disroot.org\n"
"POT-Creation-Date: 2024-11-27 12:44+0100\n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: mods/tutorial_default/craftitems.lua:4
msgid "book"
msgstr "boek"

#: mods/tutorial_default/craftitems.lua:9
msgid "coal lump"
msgstr "een klomp steenkool"

#: mods/tutorial_default/craftitems.lua:14
msgid "iron lump"
msgstr "een klomp ijzer"

#: mods/tutorial_default/craftitems.lua:19
msgid "gold lump"
msgstr "een klomp goud"

#: mods/tutorial_default/craftitems.lua:24
msgid "diamond"
msgstr "diamant"

#: mods/tutorial_default/craftitems.lua:29
msgid "steel ingot"
msgstr "stalen staaf"

#: mods/tutorial_default/craftitems.lua:34
msgid "gold ingot"
msgstr "goudstaaf"

#: mods/tutorial_default/init.lua:14
#, fuzzy
msgid ""
"[Left click]: Take/drop stack\n"
"[Right click]: Take half stack / drop 1 item\n"
"[Middle click]: Take/drop 10 items\n"
"[Esc] or [I]: Close"
msgstr ""
"[Linksklik]: pak/plaats stapel;   [Rechtsklik]: pak de helft / 1 "
"wegleggen;   [Middelklik]: pak/plaats 10"

#: mods/tutorial_default/init.lua:26
msgid "Toggle music"
msgstr ""

#: mods/tutorial_default/init.lua:33
msgid "Teleport"
msgstr ""

#: mods/tutorial_default/init.lua:35 mods/tutorial_default/nodes.lua:242
#: mods/tutorial_default/nodes.lua:305 mods/tutorial_default/nodes.lua:349
msgid "Player inventory:"
msgstr "Bezittingen:"

#: mods/tutorial_default/init.lua:39
msgid "Crafting grid:"
msgstr "Crafting raster:"

#: mods/tutorial_default/init.lua:41
msgid "Output slot:"
msgstr "Resultaat:"

#: mods/tutorial_default/nodes.lua:6
msgid "stone"
msgstr "gesteente"

#: mods/tutorial_default/nodes.lua:16
msgid "coal ore"
msgstr "steenkoolerts"

#: mods/tutorial_default/nodes.lua:25
msgid "iron ore"
msgstr "ijzererts"

#: mods/tutorial_default/nodes.lua:34
msgid "gold ore"
msgstr "gouderts"

#: mods/tutorial_default/nodes.lua:43
msgid "diamond ore"
msgstr "diamanterts"

#: mods/tutorial_default/nodes.lua:52
msgid "dirt with grass"
msgstr "aarde met gras"

#: mods/tutorial_default/nodes.lua:63
msgid "dirt"
msgstr "aarde"

#: mods/tutorial_default/nodes.lua:71
msgid "sand"
msgstr "zand"

#: mods/tutorial_default/nodes.lua:79
msgid "tree trunk"
msgstr "boomstam"

#: mods/tutorial_default/nodes.lua:89
msgid "leaves"
msgstr "bladeren"

#: mods/tutorial_default/nodes.lua:100
msgid "grass"
msgstr ""

#: mods/tutorial_default/nodes.lua:114
msgid "ladder"
msgstr "ladder"

#: mods/tutorial_default/nodes.lua:133
msgid "wooden planks"
msgstr "houten planken"

#: mods/tutorial_default/nodes.lua:140
msgid "flowing water"
msgstr "stromend water"

#: mods/tutorial_default/nodes.lua:175
msgid "water source"
msgstr "waterbron"

#: mods/tutorial_default/nodes.lua:207
msgid "torch"
msgstr "fakkel"

#: mods/tutorial_default/nodes.lua:240
msgid "Chest inventory:"
msgstr "Kistinhoud:"

#: mods/tutorial_default/nodes.lua:252
msgid "storage chest"
msgstr "opslagkist"

#: mods/tutorial_default/nodes.lua:263 mods/tutorial_default/nodes.lua:554
msgid "Chest (Rightclick to open)"
msgstr "Kist (Rechtsklikken om te openen)"

#: mods/tutorial_default/nodes.lua:292
msgid "This furnace is active and constantly burning its fuel."
msgstr "De oven is actief en verbrandt voortdurend brandstof."

#: mods/tutorial_default/nodes.lua:293 mods/tutorial_default/nodes.lua:339
msgid "Source:"
msgstr "Bronmateriaal:"

#: mods/tutorial_default/nodes.lua:295 mods/tutorial_default/nodes.lua:341
msgid "Fuel:"
msgstr "Brandstof:"

#: mods/tutorial_default/nodes.lua:297 mods/tutorial_default/nodes.lua:343
msgid "Flame:"
msgstr "Vlam:"

#: mods/tutorial_default/nodes.lua:300 mods/tutorial_default/nodes.lua:345
msgid "Progress:"
msgstr "Voortgang:"

#: mods/tutorial_default/nodes.lua:303 mods/tutorial_default/nodes.lua:347
msgid "Output slots:"
msgstr "Resultaatvakken:"

#: mods/tutorial_default/nodes.lua:338
#, fuzzy
msgid "This furnace is inactive. Please read the sign above."
msgstr ""
"Deze oven is niet actief. Lees de aanwijzingen en leer hoe de oven aangezet "
"wordt."

#: mods/tutorial_default/nodes.lua:360 mods/tutorial_default/nodes.lua:441
msgid "furnace"
msgstr "oven"

#: mods/tutorial_default/nodes.lua:371 mods/tutorial_default/nodes.lua:469
msgid "Inactive furnace (Rightclick to examine)"
msgstr "Niet actieve oven (Rechtsklikken om te onderzoeken)"

#: mods/tutorial_default/nodes.lua:398 mods/tutorial_default/nodes.lua:420
#: mods/tutorial_default/nodes.lua:496 mods/tutorial_default/nodes.lua:518
#: mods/tutorial_default/nodes.lua:636
msgid "Empty furnace (Rightclick to examine)"
msgstr "Lege oven (Rechtsklikken om te onderzoeken)"

#: mods/tutorial_default/nodes.lua:608
msgid "Active furnace (Flame used: @1%) (Rightclick to examine)"
msgstr "Actieve oven (Gebruike vlam: @1%) (Rechtsklikken om te onderzoeken)"

#: mods/tutorial_default/nodes.lua:628
msgid "Furnace without fuel (Rightclick to examine)"
msgstr "Oven zonder brandstof (Rechtsklikken om te onderzoeken)"

#: mods/tutorial_default/nodes.lua:651
msgid "cobblestone"
msgstr "keisteen"

#: mods/tutorial_default/nodes.lua:659
msgid "apple"
msgstr "appel"

#: mods/tutorial_default/tools.lua:25
msgid "wooden pickaxe"
msgstr "houten pikhouweel"

#: mods/tutorial_default/tools.lua:37
msgid "stone pickaxe"
msgstr "stenen pikhouweel"

#: mods/tutorial_default/tools.lua:49
msgid "steel pickaxe"
msgstr "stalen pikhouweel"

#: mods/tutorial_default/tools.lua:66
msgid "wooden shovel"
msgstr "houten schep"

#: mods/tutorial_default/tools.lua:80
msgid "steel axe"
msgstr "stalen bijl"

#~ msgid "stone brick"
#~ msgstr "bouwsteen"

#~ msgid "Back to start"
#~ msgstr "Terug"
