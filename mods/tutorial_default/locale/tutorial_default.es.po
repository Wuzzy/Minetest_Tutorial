msgid ""
msgstr ""
"Project-Id-Version: Tutorial (Luanti) 3.4.0\n"
"Report-Msgid-Bugs-To: Wuzzy@disroot.org\n"
"POT-Creation-Date: 2024-11-27 12:44+0100\n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: mods/tutorial_default/craftitems.lua:4
msgid "book"
msgstr "libro"

#: mods/tutorial_default/craftitems.lua:9
msgid "coal lump"
msgstr "trozo  de carbón"

#: mods/tutorial_default/craftitems.lua:14
msgid "iron lump"
msgstr "trozo de hierro"

#: mods/tutorial_default/craftitems.lua:19
msgid "gold lump"
msgstr "trozo de oro"

#: mods/tutorial_default/craftitems.lua:24
msgid "diamond"
msgstr "diamante"

#: mods/tutorial_default/craftitems.lua:29
msgid "steel ingot"
msgstr "lingote de acero"

#: mods/tutorial_default/craftitems.lua:34
msgid "gold ingot"
msgstr "lingote de oro"

#: mods/tutorial_default/init.lua:14
#, fuzzy
msgid ""
"[Left click]: Take/drop stack\n"
"[Right click]: Take half stack / drop 1 item\n"
"[Middle click]: Take/drop 10 items\n"
"[Esc] or [I]: Close"
msgstr ""
"[click izquierdo]: coger / dejar pila;   [click derecho]: coger la mitad / "
"dejar 1;   [click central]: coger / dejar 10"

#: mods/tutorial_default/init.lua:26
msgid "Toggle music"
msgstr ""

#: mods/tutorial_default/init.lua:33
msgid "Teleport"
msgstr ""

#: mods/tutorial_default/init.lua:35 mods/tutorial_default/nodes.lua:242
#: mods/tutorial_default/nodes.lua:305 mods/tutorial_default/nodes.lua:349
msgid "Player inventory:"
msgstr "inventario del jugador:"

#: mods/tutorial_default/init.lua:39
msgid "Crafting grid:"
msgstr "cuadrícula de manufactura:"

#: mods/tutorial_default/init.lua:41
msgid "Output slot:"
msgstr "hueco de productos:"

#: mods/tutorial_default/nodes.lua:6
msgid "stone"
msgstr "piedra"

#: mods/tutorial_default/nodes.lua:16
msgid "coal ore"
msgstr "veta de carbón"

#: mods/tutorial_default/nodes.lua:25
msgid "iron ore"
msgstr "veta de hierro"

#: mods/tutorial_default/nodes.lua:34
msgid "gold ore"
msgstr "veta de oro"

#: mods/tutorial_default/nodes.lua:43
msgid "diamond ore"
msgstr "veta de diamantes"

#: mods/tutorial_default/nodes.lua:52
msgid "dirt with grass"
msgstr "terrón  con hierba"

#: mods/tutorial_default/nodes.lua:63
msgid "dirt"
msgstr "terrón"

#: mods/tutorial_default/nodes.lua:71
msgid "sand"
msgstr "arena"

#: mods/tutorial_default/nodes.lua:79
msgid "tree trunk"
msgstr "tronco de árbol"

#: mods/tutorial_default/nodes.lua:89
msgid "leaves"
msgstr "hojas"

#: mods/tutorial_default/nodes.lua:100
msgid "grass"
msgstr ""

#: mods/tutorial_default/nodes.lua:114
msgid "ladder"
msgstr "escalera"

#: mods/tutorial_default/nodes.lua:133
msgid "wooden planks"
msgstr "planchas de madera"

#: mods/tutorial_default/nodes.lua:140
msgid "flowing water"
msgstr "agua corriente"

#: mods/tutorial_default/nodes.lua:175
msgid "water source"
msgstr "manantial"

#: mods/tutorial_default/nodes.lua:207
msgid "torch"
msgstr "antorcha"

#: mods/tutorial_default/nodes.lua:240
msgid "Chest inventory:"
msgstr "inventario del baúl:"

#: mods/tutorial_default/nodes.lua:252
msgid "storage chest"
msgstr "baúl"

#: mods/tutorial_default/nodes.lua:263 mods/tutorial_default/nodes.lua:554
msgid "Chest (Rightclick to open)"
msgstr "Cofre (click derecho para abrirlo)"

#: mods/tutorial_default/nodes.lua:292
msgid "This furnace is active and constantly burning its fuel."
msgstr "Este horno está encendido y quemando su combustible constantemente."

#: mods/tutorial_default/nodes.lua:293 mods/tutorial_default/nodes.lua:339
msgid "Source:"
msgstr "fuente:"

#: mods/tutorial_default/nodes.lua:295 mods/tutorial_default/nodes.lua:341
msgid "Fuel:"
msgstr "combustible:"

#: mods/tutorial_default/nodes.lua:297 mods/tutorial_default/nodes.lua:343
msgid "Flame:"
msgstr "llamas:"

#: mods/tutorial_default/nodes.lua:300 mods/tutorial_default/nodes.lua:345
msgid "Progress:"
msgstr "progreso:"

#: mods/tutorial_default/nodes.lua:303 mods/tutorial_default/nodes.lua:347
msgid "Output slots:"
msgstr "huecos de productos:"

#: mods/tutorial_default/nodes.lua:338
#, fuzzy
msgid "This furnace is inactive. Please read the sign above."
msgstr ""
"Este horno está apagado. Lee las instrucciones para aprender a encenderlo."

#: mods/tutorial_default/nodes.lua:360 mods/tutorial_default/nodes.lua:441
msgid "furnace"
msgstr "horno"

#: mods/tutorial_default/nodes.lua:371 mods/tutorial_default/nodes.lua:469
msgid "Inactive furnace (Rightclick to examine)"
msgstr "horno apagado (click derecho para examinarlo)"

#: mods/tutorial_default/nodes.lua:398 mods/tutorial_default/nodes.lua:420
#: mods/tutorial_default/nodes.lua:496 mods/tutorial_default/nodes.lua:518
#: mods/tutorial_default/nodes.lua:636
msgid "Empty furnace (Rightclick to examine)"
msgstr "horno vacío (click derecho para examinarlo)"

#: mods/tutorial_default/nodes.lua:608
msgid "Active furnace (Flame used: @1%) (Rightclick to examine)"
msgstr "horno encendido (llama usada: @1%)  (click derecho para examinarlo)"

#: mods/tutorial_default/nodes.lua:628
msgid "Furnace without fuel (Rightclick to examine)"
msgstr "horno sin combustible  (click derecho para examinarlo)"

#: mods/tutorial_default/nodes.lua:651
msgid "cobblestone"
msgstr "adoquín"

#: mods/tutorial_default/nodes.lua:659
msgid "apple"
msgstr "manzana"

#: mods/tutorial_default/tools.lua:25
msgid "wooden pickaxe"
msgstr "pico de madera"

#: mods/tutorial_default/tools.lua:37
msgid "stone pickaxe"
msgstr "pico de piedra"

#: mods/tutorial_default/tools.lua:49
msgid "steel pickaxe"
msgstr "pico de hierro"

#: mods/tutorial_default/tools.lua:66
msgid "wooden shovel"
msgstr "pala de madera"

#: mods/tutorial_default/tools.lua:80
msgid "steel axe"
msgstr "hacha de acero"

#~ msgid "stone brick"
#~ msgstr "ladrillo de piedra"

#~ msgid "Back to start"
#~ msgstr "Volver al principio"
