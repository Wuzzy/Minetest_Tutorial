-- Set to true to print area names in console
-- (needed for updating areas_list.lua)
local PRINT_AREAS = false

-- Load the areas table from the save file
function areas:load()
	local file, err = io.open(minetest.get_modpath("tutorial_areas") .. "/areas.dat", "r")
	if err then
		self.areas = self.areas or {}
		return err
	end
	self.areas = minetest.deserialize(file:read("*a"))
	if type(self.areas) ~= "table" then
		self.areas = {}
	end
	if PRINT_AREAS then
		print('--- LIST OF AREAS: ---')
		for a=1, #self.areas do
			local area = self.areas[a]
			if not area.hidden then
				local name = area.name
				if not area.name then
					minetest.log("error", "[tutorial_areas] Area no. "..a.." doesn't have a name!")
					break
				end
				print('NS("'..tostring(name)..'")')
			end
		end
		print('--- END OF LIST OF AREAS ---')
	end
	file:close()
end
