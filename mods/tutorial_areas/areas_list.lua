-- This file is not actually executed, it's just there
-- to provide the area names for the translation string collector.
-- To update this list, set PRINT_NAMES to true in internal.lua,
-- launch the tutorial and copy the list from the Luanti console
-- to here
local NS = function(s) return s end

--- LIST OF AREAS ---
NS("Start room")
NS("Jumping section")
NS("Pointing section")
NS("Using section")
NS("Inventory and Chests section")
NS("Comestibles section")
NS("Crafting and Tools section")
NS("Smelting section")
NS("Swimming section")
NS("Diving section")
NS("Waterfall section")
NS("Viscosity section")
NS("Health and Damage section")
NS("Climbing section")
NS("Mining section")
NS("Special Blocks section")
NS("Tutorial Mine")
NS("Building section")
NS("Sneaking section")
NS("Good-Bye room")
--- END OF LIST OF AREAS ---
